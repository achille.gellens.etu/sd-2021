#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "chaine.h"

int main(){
  struct chaine s;
  int c;

  init_chaine(&s);

  
  c = getchar (); // ~= scanf("%c", &c);

  while (!isspace(c)) {  // c == ' '
    add_carac(&s, c);
    c = getchar ();
  }

  print_chaine(&s);
  printf("\n");

  clear_chaine(&s);

  return 0;
}
