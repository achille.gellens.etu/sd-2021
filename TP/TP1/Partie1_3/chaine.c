#include <stdio.h>
#include <stdlib.h>
#include "chaine.h"

#ifndef CHAINE_C
#define CHAINE_C

void init_chaine(struct chaine* str) {
  init_liste_char(&str->L);
}

void clear_chaine(struct chaine* str) {
  clear_liste_char(&str->L);
}

void add_carac(struct chaine* str, char c) {
  remplacer_en_queue_liste_char(&str->L, c);
}

void print_chaine(const struct chaine* str) {
  imprimer_liste_char(&str->L);
}

#endif