#if ! defined(LISTE_CHAR_H)
#define LISTE_CHAR_H

struct maillon_char {
    char carac;
    struct maillon_char* next;
};

struct liste_char {
    struct maillon_char* tete;
    int nbelem;
};

extern void init_liste_char (struct liste_char*);
extern void clear_liste_char (struct liste_char*);
extern void ajouter_en_queue_liste_char (struct liste_char*, char);
extern void remplacer_en_queue_liste_char (struct liste_char*, char);
extern void imprimer_liste_char (const struct liste_char*);


#endif