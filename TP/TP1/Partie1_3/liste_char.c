#include <stdio.h>
#include <stdlib.h>
#include "liste_char.h"

#ifndef LISTE_CHAR_C
#define LISTE_CHAR_C

void init_liste_char (struct liste_char* lsc) {
    struct maillon_char* maillon = malloc(sizeof(struct maillon_char));

    if (maillon == NULL) {exit(EXIT_FAILURE);}

    lsc->nbelem = 0;

    maillon->next = (struct maillon_char*) 0;
    maillon->carac = '\0';

    lsc->tete = maillon;
}


void clear_liste_char (struct liste_char* lsc) {
    struct maillon_char* courant;
    struct maillon_char* suivant;

    if (lsc->tete != (struct maillon_char*)0) {courant = lsc->tete;}

    while (courant->next != (struct maillon_char*)0) {
        suivant = courant->next;
        free(courant);
        courant = suivant;
    }

    free(courant);
    
}


void ajouter_en_queue_liste_char (struct liste_char* lsc, char c) {
    struct maillon_char* ptr_maillon = lsc->tete;

    while (ptr_maillon->next != (struct maillon_char*)0 ) {
        ptr_maillon = ptr_maillon->next;
    }

    if (ptr_maillon -> carac != '\0') {
        ptr_maillon->next = malloc(sizeof(struct maillon_char));
        ptr_maillon = ptr_maillon->next;
    }

    ptr_maillon->carac = c;
    ptr_maillon->next = (struct maillon_char*)0;

    lsc->nbelem += 1;
}

void remplacer_en_queue_liste_char (struct liste_char* lsc, char c) {
    struct maillon_char* ptr_maillon = lsc->tete;

    while (ptr_maillon->carac != '\0' ) {
        ptr_maillon = ptr_maillon->next;
    }

    ptr_maillon->carac = c;

    ptr_maillon->next = malloc(sizeof(struct maillon_char));
    ptr_maillon = ptr_maillon->next;

    ptr_maillon->carac = '\0';
    ptr_maillon->next = (struct maillon_char*) 0;


    lsc->nbelem += 1;
}


void imprimer_liste_char (const struct liste_char* lsc) {
    struct maillon_char* ptr_maillon = lsc->tete;

    while (ptr_maillon->next != (struct maillon_char*)0 ) {
        printf("%c", ptr_maillon->carac);
        ptr_maillon = ptr_maillon->next;
    }

    printf("%c", ptr_maillon->carac);
}

#endif