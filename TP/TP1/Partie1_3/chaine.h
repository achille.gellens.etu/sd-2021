#ifndef CHAINE_H
#define CHAINE_H
#include "liste_char.h"

struct chaine {
  struct liste_char L;
};

extern void init_chaine();
extern void clear_chaine(struct chaine*);
extern void add_carac(struct chaine*, char);
extern void print_chaine(const struct chaine*);


#endif
