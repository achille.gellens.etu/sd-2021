#include "chaine.h"
#include <stdio.h>
#include <stdlib.h>

void init_chaine(struct chaine* str) {
  str->nb_alloue = 4;
  str->nb_carac = 0;
  str->champ = (char*)malloc(str->nb_alloue*sizeof(char));
}


void clear_chaine(struct chaine* str) {
  free(str->champ);
}

void add_carac(struct chaine* str, char carac) {
  if (str->nb_carac == str->nb_alloue) { 
    str->champ = (char*) realloc(str->champ, (str->nb_alloue + 8)*sizeof(char));
    str->nb_alloue += 8;
  }

  str->champ[str->nb_carac] = carac;
  str->nb_carac += 1;

  str->champ[str->nb_carac] = '\0';
}

void print_chaine(const struct chaine* str) {
  puts(str->champ);
}
