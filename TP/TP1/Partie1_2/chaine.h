#if ! defined(CHAINE_H)
#define CHAINE_H

struct chaine {
  char* champ;
  int nb_alloue;
  int nb_carac;
};

void init_chaine();

void clear_chaine(struct chaine*);

void  add_carac(struct chaine*, char);

void print_chaine(const struct chaine*);


#endif
