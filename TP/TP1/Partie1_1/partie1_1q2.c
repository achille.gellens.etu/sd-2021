#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

int main ()
{ 
    int c;
    int nb_carac = 0;
    int nb_alloue = 4;
    char* s;

    s = (char*) malloc((nb_alloue*sizeof(char)));

    /* Cette fonction utilise le buffer...*/

    c = getchar (); // ~= scanf("%c", &c);
    // getchar() renvoie soit un char soit EOF (Ctrl+B au clavier)
    // ce qui fait 257 possibilités donc nécessité d'utiliser un int
    // getchar() renvoie donc un int (correspondant au carac en ASCII, ou EOF = constante -1)

    while (!isspace(c)) {  // c == ' '
    // mais aussi tabulations, sauts de pages, etc... les "white-space" characters
    // variantes de is...: isdigit(c), islower(c), isupper(c)... 
      s[nb_carac] = c; // cast inutile
      c = getchar ();
      nb_carac += 1;
      if (nb_carac >= nb_alloue-1) {
	nb_alloue += 8;
	s = (char*) realloc(s, (nb_alloue)*sizeof(char) );;
      }
    }

    s[nb_carac] = '\0'; // On ajoute le caractère de fin de chaîne

    puts(s); // ~= printf("%s\n", s);

    free(s);

    return 0;
}
