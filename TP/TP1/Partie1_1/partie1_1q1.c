#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

int main ()
{ 
    int c;
    int i = 0;
    char* s;

    s = malloc(64*sizeof(char));

    /* Cette fonction utilise le buffer...*/

    c = getchar (); // ~= scanf("%c", &c);
    // getchar() renvoie soit un char soit EOF (Ctrl+B au clavier)
    // ce qui fait 257 possibilités donc nécessité d'utiliser un int
    // getchar() renvoie donc un int (correspondant au carac en ASCII, ou EOF = constante -1)

    while (!isspace(c)) {  // c == ' '
    // mais aussi tabulations, sauts de pages, etc... les "white-space" characters
    // variantes de is...: isdigit(c), islower(c), isupper(c)...
        s[i] = (char)c; // cast inutile
        c = getchar ();
        i += 1;
    }

    s[i] = '\0'; // On ajoute le caractère de fin de chaîne

    puts(s); // ~= printf("%s\n", s);

    free(s);

    return 0;
}