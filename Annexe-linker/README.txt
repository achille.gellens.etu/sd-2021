Auteur : François Boulier. Janvier 2011, 2012.

Description
===========

Ce répertoire contient une réalisation du projet LINKER, qui reproduit 
la gestion (idéalisée) de la table des symboles d'un éditeur de liens.

La table des symboles est vue comme un dictionnaire.

Différentes implantations (avec la même interface) sont données.
Farfouiller dans le répertoire : variantes-executable.

Le code privilégie la simplicité conceptuelle et sacrifie l'efficacité.
Une documentation peut être produite avec Doxygen.

Commandes utiles
================

Exécuter le code :
make
./linker *.o

Construire la documentation (voir répertoires latex ou html) :
make doc

Nettoyer le répertoire :
make clean
