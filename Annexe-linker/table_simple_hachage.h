#if ! defined (TABLE_SIMPLE_HACHAGE_H)
#define TABLE_SIMPLE_HACHAGE_H 1

#include <stdbool.h>
#include "liste_symbole.h"

/** 
 * @file
 * @brief Les types nécessaires à une implantation des tables de
 * hachage. Les alvéoles sont des listes de symboles.
 * @date mars 2011
 * @author F. Boulier
 */

/***********************************************************************
 * IMPLANTATION
 ***********************************************************************/

/**
 * @struct alveole
 * @brief Le type \p struct \p alveole fournit une implantation d'un
 * alvéole d'une table de hachage. 
 * Le champ \p L contient la liste des symboles ayant même valeur de hachage.
 */

struct alveole
{   struct liste_symbole L;   /**< symboles ayant même valeur de hachage */
};

struct table_simple_hachage;

/**
 * @typedef fonction_simple_hachage
 * @brief Le type des fonctions de hachage. Le premier paramètre est
 * l'identificateur du symbole à hacher et le second, la table pour 
 * laquelle le hachage est effectué.
 */

typedef int fonction_simple_hachage (char*, struct table_simple_hachage*);

/**
 * @struct table_simple_hachage
 * @brief Le type \p struct \p table_simple_hachage fournit une implantation
 * d'une table de hachage avec résolution des collisions par listes chaînées.
 * Le champ \p h pointe vers la fonction de hachage.
 * Le champ \p tab pointe vers le tableau des alvéolves.
 * Le champ \p N contient la taille de \p tab en nombre d'alvéoles.
 * Le champ \p nb_non_vides contient le nombre d'alvéoles qui contiennent
 * au moins un symbole
 */

struct table_simple_hachage
{   fonction_simple_hachage* h;  /**< pointeur vers la fonction de hachage */
    long nb_non_vides;           /**< nombre d'alvéoles non vides */
    long N;                      /**< nombre d'alvéoles alloués à tab */
    struct alveole* tab;         /**< la zone de stockage */
    int nb_collisions;		 /**< nombre de collisions */
};

/***********************************************************************
 * PROTOTYPES DES FONCTIONS (TYPE ABSTRAIT)
 ***********************************************************************/

extern void init_table_simple_hachage 
		(struct table_simple_hachage*, int, fonction_simple_hachage*);
extern void clear_table_simple_hachage (struct table_simple_hachage*);
extern int rechercher_symbole_dans_table_simple_hachage 
		(struct symbole**, char*, struct table_simple_hachage*);
extern void ajouter_symbole_dans_table_simple_hachage
		(struct table_simple_hachage*, struct symbole*);
extern double taux_simple_remplissage_table_simple_hachage 
		(struct table_simple_hachage*);

#endif
