#if !defined (OBJET_H)
#define OBJET_H 1

#include <stdbool.h>
#include <stdio.h>
#include "symbole.h"

/**
 * @file
 * @brief Les déclarations nécessaires à la manipulation de fichiers objets
 * @author F. Boulier
 * @date novembre 2010
 */

/***********************************************************************
 * IMPLANTATION
 ***********************************************************************/

/**
 * @struct objet
 * @brief Le type \p struct \p objet permet de manipuler les tables
 * de symboles des fichiers objets. Le champ \p filename contient le
 * nom du fichier. Le champ \p tab pointe vers un tableau de symboles.
 * Le champ \p alloc contient le nombre d'entrées allouées à \p tab.
 * Le champ \p size contient le nombre d'entrées effectivement utilisées.
 * Les chaînes de caractères et les tableau de symboles sont alloués
 * dynamiquement.
 */

struct objet
{   char* filename;        /**< le nom du fichier */
    int   size;            /**< le nombre d'entrées utilisées */
    int   alloc;           /**< le nombre d'entrées allouées */
    struct symbole* tab;   /**< le tableau des symboles */
};

/***********************************************************************
 * PROTOTYPES DES FONCTIONS (TYPE ABSTRAIT)
 ***********************************************************************/

extern void init_objet (struct objet*);
extern void clear_objet (struct objet*);
extern void charger_objet (struct objet*, char*, FILE*);
extern void set_objet (struct objet*, struct objet*);

extern bool est_fichier_objet (char*);
extern bool est_fichier_bibliotheque (char*);

#endif 
