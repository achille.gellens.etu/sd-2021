#if !defined (SYMBOLE_H)
#define SYMBOLE_H 1

#include <stdbool.h>

/**
 * @file
 * @brief Les déclarations nécessaires à la manipulation des symboles.
 * @author F. Boulier
 * @date novembre 2010
 */

/***********************************************************************
 * IMPLANTATION
 ***********************************************************************/

/**
 * @struct symbole
 * @brief Le type \p struct \p symbole permet de représenter un symbole.
 * Le champ \p type contient le type du symbole (voir la documentation
 * de l'utilitaire \p nm). Le champ \p ident contient l'identificateur.
 * Le champ \p nbdef permet de compter les définitions.
 * Les chaînes de caractères sont allouées dynamiquement.
 */

struct symbole
{   char type;                 /**< le type du symbole */
    char* ident;               /**< son identificateur */
    int nbdef;		       /**< le nombre de définitions */
};

/***********************************************************************
 * PROTOTYPES DES FONCTIONS (TYPE ABSTRAIT)
 ***********************************************************************/

extern void init_symbole (struct symbole*);
extern void clear_symbole (struct symbole*);
extern void imprimer_symbole (struct symbole*);
extern void set_symbole (struct symbole*, struct symbole*);
extern void set_symbole_tic (struct symbole*, char, char*, int);
extern void ajouter_definition_symbole (struct symbole*);
extern void changer_type_symbole (struct symbole*, char);
extern void changer_nbdef_symbole (struct symbole*, int);
extern bool est_indefini_symbole (struct symbole*);
extern bool est_faible_symbole (struct symbole*);
extern bool est_global_et_defini_symbole (struct symbole*);
extern bool est_defini_symbole (struct symbole*);
extern bool est_local_symbole (struct symbole*);
extern bool est_duplique_symbole (struct symbole*);

#endif 
