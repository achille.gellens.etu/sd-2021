#include <stdlib.h>
#include <string.h>
#include "stats.h"
#include "error.h"

/**
 * @file
 * @brief Implantation des statistiques destinées à mesurer l'efficacité 
 * de la table des symboles des exécutables
 * @author F. Boulier
 * @date novembre 2010
 */

/**
 * @brief Constructeur. Initialise \p stats. Cette fonction devrait être
 * appelée avant tout autre utilisation de \p stats.
 * @param[out] stats une structure
 */

void init_stats (struct stats* stats)
{
    stats->nbcomp = 0;
    stats->nbget = 0;
    stats->nbsym = 0;
    stats->f = fopen (STATS_FILENAME, "w");
    if (stats->f == (FILE*)0)
	error ("init_stats", __FILE__, __LINE__);
    commenter_stats (stats, "nb acces dico | nb comp. chaines | nb symboles");
}

/**
 * @brief Destructeur. Libère les ressources consommées par \p stats. Cette
 * fonction devrait être appelée après la dernière utilisation de \p stats.
 * @param[in,out] stats une structure
 */

void clear_stats (struct stats* stats)
{
    if (stats->f != (FILE*)0)
	fclose (stats->f);
}

/**
 * @brief Imprime un commentaire sur une nouvelle ligne dans le fichier
 * de stats.
 * @param[in,out] stats une structure
 * @param[in] comment une chaîne de caractères
 */

void commenter_stats (struct stats* stats, char* comment)
{
    fprintf (stats->f, "# %s\n", comment);
}

/**
 * @brief Incrémente le compteur d'accès au dictionnaire (nombre
 * d'enregistrements et de recherches).
 * @param[in,out] stats une structure
 * @param[in] n le nombre à rajouter
 */

void incrementer_nb_acces_dico_stats (struct stats* stats, int n)
{
    stats->nbget += n;
}

/**
 * @brief Incrémente le compteur de comparaisons de chaînes de caractères.
 * @param[in,out] stats une structure
 * @param[in] n le nombre à rajouter
 */

void incrementer_nb_comp_chaines_stats (struct stats* stats, int n)
{
    stats->nbcomp += n;
}

/**
 * @brief Incrémente le nombre d'éléments présents dans le dictionnaire.
 * @param[in,out] stats une structure
 * @param[in] n le nombre à rajouter
 */

void incrementer_nb_symboles_stats (struct stats* stats, int n)
{
    stats->nbsym += n;
}


/**
 * @brief Imprime une ligne du fichier de stats.
 * Nombre d'appels à \p get, nombre d'appels à \p get ayant retourné \p true,
 * nombre de comparaisons de chaînes, nombre de symboles dans la table.
 * @param[in,out] stats une structure.
 */

void imprimer_stats (struct stats* stats)
{
    if (stats->f != (FILE*)0)
	fprintf (stats->f, "%d\t%d\t%d\n", 
		 stats->nbget, stats->nbcomp, stats->nbsym);
}

