#include <stdlib.h>
#include <string.h>
#include "symtable.h"
#include "error.h"

/**
 * @file
 * @brief Implantation des tables des symboles des exécutables
 * @author F. Boulier
 * @date novembre 2010
 */

/**
 * @brief Constructeur. Initialise \p table. Cette fonction devrait
 * être appelée avant toute autre utilisation de \p table.
 * @param[out] table une table des symboles
 */

void init_symtable (struct symtable* table)
{
    table->size = 0;
    table->alloc = 0;
    table->tab = (struct symbole_avec_compteur*)0;
    init_stats (&table->mesures);
}

/**
 * @brief Destructeur. Libère les ressources consommées par \p table.
 * Cette fonction devrait être appelée après la dernière utilisation
 * de \p table.
 * @param[in,out] table une table des symboles
 */


void clear_symtable (struct symtable* table)
{   int i;

    if (table->tab != (struct symbole_avec_compteur*)0)
    {	for (i = 0; i < table->alloc; i++)
	    clear_symbole_avec_compteur (&table->tab [i]);
	free (table->tab);
    }
    clear_stats (&table->mesures);
}

static void realloc_symtable (struct symtable* table, int n)
{   struct symbole_avec_compteur* newtab;

    if (table->alloc < n)
    {   newtab = (struct symbole_avec_compteur*)realloc
		    (table->tab, n * sizeof (struct symbole_avec_compteur));
        if (newtab == (struct symbole_avec_compteur*)0)
            error ("realloc_symtable", __FILE__, __LINE__);
	table->tab = newtab;
    }
    while (table->alloc < n)
    {   init_symbole_avec_compteur (&table->tab [table->alloc]);
        table->alloc += 1;
    }
}

/**
 * @brief Enregistre le symbole \p sym dans 
 * \p table. 
 * Supposons \p sym global. Si un
 * symbole de même nom, indéfini, est déjà présent, ce
 * symbole devient défini. Si un symbole de même nom, défini,
 * est déjà présent, on a affaire à une définition multiple.
 * @param[in,out] table une table des symboles
 * @param[in] sym un symbole
 */

void put_symbole_symtable (struct symtable* table, struct symbole* sym)
{   struct symbole_avec_compteur* symcpt;
    bool found;
    int a, b, i, pos, comp;

/* On compte la recherche initiale comme un get_symbole_symtable */

    table->mesures.nbget += 1;

    a = 0;
    b = table->size;
    pos = 0;
    found = false;
    while (a < b && !found)
    {   i = (a + b) / 2;
	symcpt = &table->tab [i];
        table->mesures.nbcomp += 1;
        comp = strcmp (symcpt->sym.ident, sym->ident);
        if (comp == 0)
        {   if (est_global_et_defini_symbole (sym))
            {   if (est_indefini_symbole (&symcpt->sym))
                    set_symbole_avec_compteur_type (symcpt, sym->type);
                else
                    symcpt->compteur += 1;
            }
	    pos = i;
            found = true;
        } else if (comp < 0)
	{   pos = i+1;
	    a = i+1;
	} else
	{   pos = i;
	    b = i;
	}
    }

    if (! found)
    {   if (table->size >= table->alloc)
	   realloc_symtable (table, 2 * table->alloc + 1);
	for (i = table->size; i > pos; i--)
        {   table->mesures.nbcomp += 1;
	    set_symbole_avec_compteur (&table->tab [i], &table->tab [i-1]);
	}
        set_symbole_avec_compteur_symbole_compteur 
					(&table->tab [pos], sym, 1);
        table->size += 1;
        table->mesures.nbsym = table->size;

	for (i = 1; i < table->size; i++)
	{   comp = strcmp (table->tab [i-1].sym.ident,
			   table->tab [i].sym.ident);
	    if (comp >= 0)
		error ("put_symbole_symtable", __FILE__, __LINE__);
	}
    } else
	table->mesures.nbsucc += 1;
    imprimer_stats (&table->mesures);
}

/**
 * @brief Recherche un symbole d'identificateur \p ident 
 * dans la table des symboles de \p table
 * Retourne l'adresse de ce symbole ou le pointeur nul si le symbole
 * est inconnu.
 * @param[in] ident un identificateur de symbole
 * @param[in] table une table des symboles
 */

struct symbole* get_symbole_symtable (char* ident, struct symtable* table)
{   bool found;
    struct symbole_avec_compteur* symcpt;
    struct symbole* sym;
    int a, b, i, comp;

    table->mesures.nbget += 1;

    a = 0;
    b = table->size;
    found = false;
    while (a < b && !found)
    {   i = (a + b) / 2;
	symcpt = &table->tab [i];
        table->mesures.nbcomp += 1;
        comp = strcmp (symcpt->sym.ident, ident);
        if (comp == 0)
            found = true;
        else if (comp < 0)
	    a = i+1;
	else
	    b = i;
    }

    if (found)
    {	sym = &table->tab [i].sym;
	table->mesures.nbsucc += 1;
    } else
	sym = (struct symbole*)0;
    imprimer_stats (&table->mesures);
    return sym;
}

/*
 * @brief Imprime le résultat de l'édition des liens
 * @param[in] table une table des symboles
 */

int synthese_symtable (struct symtable* table)
{   struct symbole* sym;
    bool ok, first;
    int i;

    ok = true;
    first = true;
    for (i = 0; i < table->size; i++)
    {	if (est_indefini_symbole (&table->tab [i].sym))
	{   if (first)
		printf ("symboles indefinis\n");
	    printf ("\t%s\n", table->tab [i].sym.ident);
	    ok = false;
	    first = false;
	}
    }
    sym = get_symbole_symtable ("main", table);
    if (sym == (struct symbole*)0)
    {	if (first)
	    printf ("symboles indefinis\n");
	printf ("\t%s\n", "main");
	ok = false;
	first = false;
    }
    first = true;
    for (i = 0; i < table->size; i++)
    {   if (! est_faible_symbole (&table->tab [i].sym) &&
					table->tab [i].compteur >= 2)
	{   if (first)
                printf ("symboles dupliques\n");
	    printf ("\t%s\n", table->tab [i].sym.ident);
	    ok = false;
	    first = false;
	}
    }
    if (ok)
	printf ("edition des liens reussie\n");
    return ok ? 0 : 1;
}

