#if ! defined (TABLE_DOUBLE_HACHAGE_H)
#define TABLE_DOUBLE_HACHAGE_H 1

#include <stdbool.h>
#include "symbole.h"

/** 
 * @file
 * @brief Les types nécessaires à l'implantation d'une table de
 * hachage, avec un adressage est ouvert et la technique du double hachage.
 * Les valeurs stockées dans les tables sont des symboles.
 * @date décembre 2010
 * @author F. Boulier
 */

/***********************************************************************
 * IMPLANTATION
 ***********************************************************************/

/**
 * @struct valeur_double_hachage
 * @brief Le type \p struct \p valeur_double_hachage donne le type du
 * résultat des fonctions de hachage. Le champ \p h1
 * donne l'indice initial dans la table. Le champ \p h2 donne
 * l'incrément, nécessaire à la technique du double hachage.
 */

struct valeur_double_hachage
{   int h1;        /**< indice initial dans la table */
    int h2;        /**< décalage (double hachage) */
};

/**
 * @enum etat_alveole
 * @brief Le type \p enum \p etat_alveole donne l'état d'un alvéole de la
 * table de hachage. Les différentes valeurs possibles sont 
 * \p alveole_vide, \p alveole_occupe, et \p alveole_detruit.
 */

enum etat_alveole { alveole_vide, alveole_occupe, alveole_detruit };

/**
 * @struct alveole
 * @brief Le type \p struct \p alveole fournit une implantation d'un
 * alvéole d'une table de hachage. 
 * Le champ \p etat donne l'état de l'alvéole. 
 * Le champ \p sym contient la valeur (le symbole).
 */

struct alveole
{   struct symbole sym;                   /**< valeur si etat = occupe */
    enum etat_alveole etat;		  /**< l'état de l'alvéole */
};

struct table_double_hachage;

/**
 * @typedef fonction_double_hachage
 * @brief Le type des fonctions de hachage. Le premier paramètre est
 * un identificateur de symbole, le second est la table pour laquelle 
 * le hachage est effectué.
 */

typedef struct valeur_double_hachage 
		fonction_double_hachage (char*, struct table_double_hachage*);

/**
 * @struct table_double_hachage
 * @brief Le type \p struct \p table_double_hachage fournit une implantation
 * d'une table de hachage avec la technique du double hachage.
 * Le champ \p h pointe vers la fonction de hachage.
 * Le champ \p tab pointe vers le tableau des alvéolves.
 * Le champ \p N contient la taille de \p tab (un nombre premier).
 * Le champ \p nb_non_vides contient le nombre d'alvéoles qui ne sont
 * pas dans l'état \p alveole_vide.
 */

struct table_double_hachage
{   fonction_double_hachage* h;  /**< pointeur vers la fonction de hachage */
    struct alveole* tab;         /**< la zone de stockage */
    long N;                      /**< nombre d'alvéoles alloués à tab */
    long nb_non_vides;           /**< nombre d'alvéoles non vides */
    int nb_collisions;		 /**< nombre de collisions */
    double nb_collisions_th;
    int lmax_sondages;		 /**< la longueur maximale des sondages */
};

/***********************************************************************
 * PROTOTYPES DES FONCTIONS (TYPE ABSTRAIT)
 ***********************************************************************/

extern void init_table_double_hachage 
		(struct table_double_hachage*, int, fonction_double_hachage*);
extern void clear_table_double_hachage (struct table_double_hachage*);
extern int rechercher_symbole_dans_table_double_hachage 
		(struct symbole**, char*, struct table_double_hachage*);
extern void ajouter_symbole_dans_table_double_hachage
		(struct table_double_hachage*, struct symbole*);
extern double taux_double_remplissage_table_double_hachage 
		(struct table_double_hachage*);

#endif
