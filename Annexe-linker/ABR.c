#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include "ABR.h"
#include "error.h"

/**
 * @file 
 * @brief Implantation des arbres binaires de recherche (ABR) et 
 * des piles d'arbres binaires de recherche.
 * @author F. Boulier
 * @date novembre 2010
 */

/**
 * @brief Constructeur. Initialise \p ABR. Cette fonction devrait être
 * appelée avant toute autre utilisation de \p ABR.
 * @param[out] ABR un arbre binaire de recherche
 */

void init_ABR (struct ABR* ABR)
{
    ABR->gauche = (struct ABR*)0;
    ABR->droit = (struct ABR*)0;
    init_symbole (&ABR->value);
#if defined (AVL)
    ABR->hauteur_gauche = 0;
    ABR->hauteur_droit = 0;
#endif
}

/**
 * @brief Destructeur. Libère les ressources consommées par \p ABR.
 * Cette fonction devrait être appelée après la dernière utilisation de
 * \p ABR. Note : les structures allouées aux deux sous-arbres sont 
 * libérées (\p free) mais la structure elle-même ne l'est pas.
 * @param[in,out] ABR un arbre binaire de recherche
 */

void clear_ABR (struct ABR* ABR)
{
    if (ABR->gauche != (struct ABR*)0)
    {	clear_ABR (ABR->gauche);
	free (ABR->gauche);
    }
    if (ABR->droit != (struct ABR*)0)
    {   clear_ABR (ABR->droit);
	free (ABR->droit);
    }
    clear_symbole (&ABR->value);
}

/**
 * @brief Alloue (\p malloc) un \p struct \p ABR, initialise ses deux
 * sous-arbres avec l'arbre vide. La structure allouée est retournée.
 */

struct ABR* new_ABR (void)
{   struct ABR* ABR;

    ABR = (struct ABR*)malloc (sizeof (struct ABR));
    if (ABR == (struct ABR*)0)
        error ("new_ABR", __FILE__, __LINE__);
    init_ABR (ABR);
    return ABR;
}

static int max (int a, int b)
{
    return a < b ? b : a;
}

/**
 * @brief Retourne la hauteur de \p ABR.
 * @param[in] ABR un arbre binaire de recherche
 */

int hauteur_ABR (struct ABR* ABR)
{   int a, b;

    if (ABR == (struct ABR*)0)
	return 0;
    else
    {	
#if defined (AVL)
	a = ABR->hauteur_gauche;
	b = ABR->hauteur_droit;
#else
	a = hauteur_ABR (ABR->gauche);
	b = hauteur_ABR (ABR->droit);
#endif
	return 1 + max (a, b);
    }
}

static void print_ABR (int p, struct ABR* A)
{   

    if (A != (struct ABR*)0)
    {	print_ABR (p+1, A->gauche);
	printf ("%s\n", A->value.ident);
	print_ABR (p+1, A->droit);
    }
}

/**
 * @brief Imprime la structure de A sur la sortie standard.
 * @param[in] A un arbre binaire de recherche
 */

void imprimer_ABR (struct ABR* A)
{
    print_ABR (0, A);
}

static void dot_print_ABR (FILE* f, struct ABR* A)
{
    if (A->gauche != (struct ABR*)0)
    {	fprintf (f, "    %s -> %s;\n", A->value.ident, A->gauche->value.ident);
	dot_print_ABR (f, A->gauche);
    }
    if (A->droit != (struct ABR*)0)
    {	fprintf (f, "    %s -> %s;\n", A->value.ident, A->droit->value.ident);
	dot_print_ABR (f, A->droit);
    }
}

/**
 * @brief Construit une représentation graphique de l'ABR \p A
 * dans le fichier \p fname.pdf. Repose sur l'utilitaire \p dot.
 * @param[in] fname un nom de fichier (sans l'extension)
 * @param[in] A un ABR
 */

void dot_imprimer_ABR (char* fname, struct ABR* A)
{   char buffer [256];
    FILE* f;

    sprintf (buffer, "%s.dot", fname);
    f = fopen (buffer, "w");
    if (f == (FILE*)0)
	error ("dot_imprimer_ABR", __FILE__, __LINE__);
    fprintf (f, "digraph G {\n");
    if (A != NIL)
	dot_print_ABR (f, A);
    fprintf (f, "}\n");
    fclose (f);
    sprintf (buffer, "dot -Tpdf %s.dot -Grankdir=LR -o %s.pdf", fname, fname);
    system (buffer);
}

#if defined (AVL)

/*
 * Appelé rotation_gauche dans CLRS, page 272
 * Si on imagine un point fictif au centre du dessin, tous les noeuds
 * et les sous-arbres effectuent un mouvement de rotation, autour de
 * ce centre, dans le sens trigonométrique (= inverse des aiguilles d'une
 * montre).
 *
 * Les hauteurs de A et B sont mises à jour

            A                                     B
           / \                                   / \
      alpha   B               -->               A   gamma
             / \                               / \
         beta   gamma                     alpha   beta
 */

static struct ABR* rotation_sens_trigonometrique_AVL (struct ABR* A)
{
    struct ABR *B, *beta;
    int h_beta;

    B = A->droit;
    beta = B->gauche;
    h_beta = B->hauteur_gauche;

    A->droit = beta;
    A->hauteur_droit = h_beta;

    B->gauche = A;
    B->hauteur_gauche = hauteur_ABR (A);

    return B;
}

/*
 * Appelé rotation_droite dans CLRS page 272
 *
 * Les hauteurs de B et C sont mises à jour
 *
            B                                      C
           / \                                    / \
          C   gamma           -->            delta   B
         / \                                        / \
    delta   epsilon                          epsilon   gamma

 */

static struct ABR* rotation_sens_trigonometrique_inverse_AVL (struct ABR* B)
{
    struct ABR *C, *epsilon;
    int h_epsilon;

    C = B->gauche;
    epsilon = C->droit;
    h_epsilon = C->hauteur_droit;

    B->gauche = epsilon;
    B->hauteur_gauche = h_epsilon;
    C->droit = B;
    C->hauteur_droit = hauteur_ABR (B);

    return C;
}

/*
 * brief Cette fonction, qui n'est disponible que si la variable
 * \p AVL du préprocesseur est définie, rééquilibre l'arbre AVL
 * \p *racine après ajout d'une nouvelle feuille.
 * L'arbre était supposée équilibré avant l'ajout.
 * La pile P contient la suite des noeuds (la branche) qui conduit de la
 * nouvelle feuille vers la racine. 
 * La fonction recalcule les champs \p hauteur_gauche et \p hauteur_droit
 * le long de la nouvelle branche.
 * Après rééquilibrage, la différence des hauteurs des sous-arbres gauche
 * et droit de tout noeud de l'arbre, n'excède pas 1 en valeur absolue.
 * @param[out] racine l'adresse de la racine de l'arbre
 * @param[in,out] P une pile d'arbres binaires de recherche
 */

static void reequilibrer_AVL (struct ABR** racine, struct pile_ABR* P)
{   struct ABR *A, *B, *A_orig, *A_new;

    if (est_vide_pile_ABR (P))
	return;

    depiler_ABR (&A, P);
    A_orig = A;
    A_new = A;
    A->hauteur_gauche = hauteur_ABR (A->gauche);
    A->hauteur_droit = hauteur_ABR (A->droit);

    while (! est_vide_pile_ABR (P))
    {	depiler_ABR (&A, P);
/*
 * A_orig = l'ancienne valeur d'une des sous-arbres de A
 * A_new  = la nouvelle valeur de ce sous-arbre
 *
 * On commence par mettre à jour le sous-arbre de A concerné ainsi
 * que sa hauteur.
 */
	if (A->gauche == A_orig)
	{   A->gauche = A_new;
	    A->hauteur_gauche = hauteur_ABR (A->gauche);
	} else
	{   A->droit = A_new;
	    A->hauteur_droit = hauteur_ABR (A->droit);
	}
/*
 * Nouvelle valeur de A_orig (pour la prochaine itération)
 */
	A_orig = A;

	if (A->hauteur_droit - A->hauteur_gauche >= 2)
	{
/*
 * Le sous-arbre droit B de A est trop haut. 
 */
	    B = A->droit;
/*
 * Comme l'arbre est obtenu par ajout d'une seule feuille dans un
 * arbre équilibré, il est impossible que les deux sous-arbres
 * du sous-arbre droit B de A (forcément celle où l'ajout a eu lieu)
 * aient même hauteur.
 */
	    if (B->hauteur_gauche == B->hauteur_droit)
		error ("reequilibre_AVL", __FILE__, __LINE__);
/*
 * On a une ou deux rotations à faire
 */
	    if (B->hauteur_gauche > B->hauteur_droit)
	    {   A->droit = rotation_sens_trigonometrique_inverse_AVL (B);
		A->hauteur_droit = hauteur_ABR (A->droit);
	    }
/*
 * nouvelle valeur de A
 */
	    A_new = rotation_sens_trigonometrique_AVL (A);
	} else if (A->hauteur_gauche - A->hauteur_droit >= 2)
	{   
/*
 * Le sous-arbre gauche est trop haute. Voir plus haut.
 * Traitement et commentaires s'obtiennent par symétrie. 
 */
	    B = A->gauche;
	    if (B->hauteur_gauche == B->hauteur_droit)
                error ("reequilibre_AVL", __FILE__, __LINE__);
	    if (B->hauteur_droit > B->hauteur_gauche)
	    {	A->gauche = rotation_sens_trigonometrique_AVL (B);
		A->hauteur_gauche = hauteur_ABR (A->gauche);
	    }
	    A_new = rotation_sens_trigonometrique_inverse_AVL (A);
	} else
	    A_new = A;

	if (A_new->hauteur_droit - A_new->hauteur_gauche >= 2 ||
		A_new->hauteur_gauche - A_new->hauteur_droit >= 2)
	    error ("reequilibre_AVL", __FILE__, __LINE__);
    }
/*
 * La racine de l'arbre a pu être modifiée
 */
    *racine = A_new;
}

#endif

/**
 * @brief
 * Recherche un symbole d'identificateur \p clef dans un ABR de racine
 * \p racine. Si un tel symbole existe, son adresse est affectée à
 * \p *sym. Sinon, \p *sym reçoit zéro. La fonction retourne le nombre
 * de comparaisons de chaînes de caractères effectuées.
 * @param[out] sym un pointeur sur un symbole
 * @param[in] clef une chaîne de caractères
 * @param[in] racine un ABR
 */

int rechercher_symbole_dans_ABR 
		(struct symbole** sym, char* clef, struct ABR* racine)
{   struct ABR* noeud;
    int comp, nbcomp;
    bool found;

    found = false;
    noeud = racine;
    nbcomp = 0;
    while (noeud != NIL && !found)
    {   comp = strcmp (noeud->value.ident, clef);
	nbcomp += 1;
	if (comp == 0)
	    found = true;
	else if (comp < 0)
	    noeud = noeud->droit;
	else
	    noeud = noeud->gauche;
    }
    if (found)
	*sym = &noeud->value;
    else
	*sym = (struct symbole*)0;
    return nbcomp;
}

/**
 * @brief
 * Ajoute une copie de \p sym à l'ABR de racine \p racine.
 * On suppose que \p sym n'est pas déjà présent dans racine.
 * @param[in,out] racine un ABR
 * @param[in] sym un symbole
 */

#if ! defined (AVL)
struct ABR* ajouter_symbole_dans_ABR (struct ABR* racine, struct symbole* sym)
{   struct ABR* pred;
    struct ABR* succ;
    struct ABR* resultat;
    int comp;
    bool found;

    if (racine == NIL)
    {   resultat = new_ABR ();
	set_symbole (&resultat->value, sym);
    } else
    {   resultat = racine;
	pred = NIL;
	succ = racine;
	found = false;
	while (succ != NIL && !found)
	{   pred = succ;
	    comp = strcmp (succ->value.ident, sym->ident);
	    if (comp == 0)
		found = true;
	    else if (comp < 0)
	        succ = succ->droit;
	    else
		succ = succ->gauche;
	}
	if (found)
	    error ("ajouter_symbole_dans_ABR", __FILE__, __LINE__);
	else if (comp < 0)
	{   pred->droit = new_ABR ();
	    set_symbole (&pred->droit->value, sym);
	} else
	{   pred->gauche = new_ABR ();
	    set_symbole (&pred->gauche->value, sym);
        }
    }
    return resultat;
}
#else
struct ABR* ajouter_symbole_dans_ABR (struct ABR* racine, struct symbole* sym)
{   struct ABR* pred;
    struct ABR* succ;
    struct ABR* resultat;
    int comp;
    bool found;
    struct pile_ABR pile;

    if (racine == NIL)
    {   resultat = new_ABR ();
	set_symbole (&resultat->value, sym);
    } else
    {   resultat = racine;
	pred = NIL;
	succ = racine;
	found = false;
	init_pile_ABR (&pile);
	while (succ != NIL && !found)
	{   empiler_ABR (&pile, succ);
	    pred = succ;
	    comp = strcmp (succ->value.ident, sym->ident);
	    if (comp == 0)
		found = true;
	    else if (comp < 0)
	        succ = succ->droit;
	    else
		succ = succ->gauche;
	}
	if (found)
	    error ("ajouter_symbole_dans_ABR", __FILE__, __LINE__);
	else if (comp < 0)
	{   pred->droit = new_ABR ();
	    set_symbole (&pred->droit->value, sym);
	    pred->hauteur_droit += 1;
	} else
	{   pred->gauche = new_ABR ();
	    set_symbole (&pred->gauche->value, sym);
	    pred->hauteur_gauche += 1;
        }
	reequilibrer_AVL (&resultat, &pile);
	clear_pile_ABR (&pile);
    }
    return resultat;
}
#endif

/***********************************************************************
 * PILES D'ABR
 ***********************************************************************/

/**
 * @brief Constructeur. Initialise la pile P.
 * Cette fonction devrait être appelée avant toute autre utilisation
 * de P. La pile initialisée est vide. 
 * @param[out] P une pile
 */

void init_pile_ABR (struct pile_ABR* P)
{
    P->tab = (struct ABR**)0;
    P->alloc = 0;
    P->sp = -1;
}

/**
 * @brief Vide la pile P.
 * @param[out] P une pile
 */

void vider_pile_ABR (struct pile_ABR* P)
{
    P->sp = -1;
}

/**
 * @brief Destructeur. Libère les ressources utilisées par la pile P.
 * Cette fonction devrait être appelée après la dernière utilisation
 * de la pile P.
 * @param[out] P une pile
 */

void clear_pile_ABR (struct pile_ABR* P)
{
    if (P->tab != (struct ABR**)0)
	free (P->tab);
}

/**
 * @brief Retourne \p true si P est vide.
 * @param[in] P une pile
 */

bool est_vide_pile_ABR (struct pile_ABR* P)
{
    return P->sp == -1;
}

/**
 * @brief Retourne \p true si la pile P est pleine.
 * @param[in] P une pile
 */

bool est_pleine_pile_ABR (struct pile_ABR* P)
{   int newalloc;
    struct ABR** newtab;
    bool b;

    if (P->sp + 1 < P->alloc)
        b = false;
    else
    {   newalloc = 2 * P->alloc + 1;
        newtab = (struct ABR**)realloc (P->tab, newalloc * sizeof(struct ABR*));
        if (newtab == (struct ABR**)0)
            b = true;
        else
        {   P->alloc = newalloc;
            P->tab = newtab;
            b = false;
        }
    }
    return b;
}

/**
 * @brief Empile une copie de B dans P.
 * @param[out] P une pile
 * @param[in] B un arbre binaire de recherche
 */

void empiler_ABR (struct pile_ABR* P, struct ABR* B)
{
    if (est_pleine_pile_ABR (P))
	error ("empiler_ABR", __FILE__, __LINE__);
    P->sp += 1;
    P->tab [P->sp] = B;
}

/**
 * @brief Dépile un élément de P et en affecte une copie à B.
 * @param[out] B un arbre binaire de recherche
 * @param[in,out] P une pile
 */

void depiler_ABR (struct ABR** B, struct pile_ABR* P)
{
    if (est_vide_pile_ABR (P))
	error ("depiler_ABR", __FILE__, __LINE__);
    *B = P->tab [P->sp];
    P->sp -= 1;
} 

